from .models import Organization, Location, Donation, Contact, Country
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from .serializers import OrganizationSerializer, LocationSerializer, ContactSerializer, DonationSerializer, CountrySerializer
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.utils import timezone


class OrganizationViewSet(viewsets.ViewSet):
    """
    list:
    Return a list of all the verified organizations only.

    retrieve:
    Return the verfied given organization.

    unfiltered:
    Return a list of all organizations
    """

    def list(self, request):
        queryset = Organization.objects.filter(verify_status='V')
        serializer = OrganizationSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Organization.objects.filter(verify_status='V')
        organization = get_object_or_404(queryset, pk=pk)
        serializer = OrganizationSerializer(organization)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def unfiltered(self, request):
        queryset = Organization.objects.all()
        serializer = OrganizationSerializer(queryset, many=True)
        return Response(serializer.data)


class OrganizationContactViewSet(viewsets.ViewSet):

    def list(self, request, organization_pk=None):
        queryset = Contact.objects.filter(organization=organization_pk)
        serializer = ContactSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, organization_pk=None):
        queryset = Contact.objects.filter(pk=pk, organization=organization_pk)
        contact = get_object_or_404(queryset, pk=pk)
        serializer = ContactSerializer(contact)
        return Response(serializer.data)


class OrganizationDonationViewSet(viewsets.ViewSet):

    def list(self, request, organization_pk=None):
        queryset = Donation.objects.filter(organizations=organization_pk)
        serializer = DonationSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, organization_pk=None):
        queryset = Donation.objects.filter(
            pk=pk, organizations=organization_pk)
        donation = get_object_or_404(queryset, pk=pk)
        serializer = LocationSerializer(donation)
        return Response(serializer.data)


class OrganizationLocationViewSet(viewsets.ViewSet):

    def list(self, request, organization_pk=None):
        queryset = Location.objects.filter(organization=organization_pk)
        serializer = LocationSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, organization_pk=None):
        queryset = Location.objects.filter(pk=pk, organization=organization_pk)
        location = get_object_or_404(queryset, pk=pk)
        serializer = LocationSerializer(location)
        return Response(serializer.data)


class CountryViewSet(viewsets.ViewSet):

    """
    list:
    Return a list of all countries

    retrieve:
    Return all countries
    """

    def list(self, request):
        queryset = Country.objects.all()
        serializer = CountrySerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Country.objects.all()
        country = get_object_or_404(queryset, pk=pk)
        serializer = CountrySerializer(country)
        return Response(serializer.data)


class CountryLocationViewSet(viewsets.ViewSet):

    """
    list:
    Return a list of all locations of selected country

    retrieve:
    Return all locations of selected country
    """

    def list(self, request, country_pk=None):
        queryset = Location.objects.filter(
            address__city__region__country=country_pk)
        serializer = LocationSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, country_pk=None):
        queryset = Location.objects.filter(
            pk=pk, address__city__region__country=country_pk)
        country_locations = get_object_or_404(queryset, pk=pk)
        serializer = LocationSerializer(country_locations)
        return Response(serializer.data)


class LocationViewSet(viewsets.ViewSet):
    """
    list:
    Return a list of the valid locations with verified organizations.

    retrieve:
    Return the valid given location.

    unfiltered:
    Return a list of all locations
    """

    def list(self, request):
        queryset = Location.objects.filter(organization__verify_status='V').exclude(
            is_permanent=False, valid_until__lt=timezone.now())
        serializer = LocationSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Location.objects.filter(organization__verify_status='V').exclude(
            is_permanent=False, valid_until__lt=timezone.now())
        location = get_object_or_404(queryset, pk=pk)
        serializer = LocationSerializer(location)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def unfiltered(self, request):
        queryset = Location.objects.all()
        serializer = LocationSerializer(queryset, many=True)
        return Response(serializer.data)


class ApiRootViewSet(viewsets.ViewSet):
    """
    list:
    Redirect to the index page.
    """

    def list(self, request):
        return HttpResponseRedirect('/')
