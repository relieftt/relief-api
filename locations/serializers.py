from .models import Organization, Address, Location, Donation, Contact, Country
from rest_framework import serializers


class DonationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Donation
        fields = ('item', 'description')


class ContactSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contact
        fields = ('name', 'phone_call_code', 'phone_area_code', 'phone_prefix', 'phone_line_code',
                  'sms_allowed', 'whatsapp_allowed', 'snapchat_allowed', 'telegram_allowed')


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = ('address_1', 'address_2', 'city_name',
                  'region_name', 'country_name')


class OrganizationSerializer(serializers.ModelSerializer):
    primary_address = AddressSerializer(read_only=True)
    contacts = ContactSerializer(many=True)
    donations = DonationSerializer(many=True)

    class Meta:
        model = Organization
        fields = ('slug', 'title', 'email', 'status', 'is_ngo', 'website',
                  'facebook', 'twitter', 'primary_address', 'contacts', 'donations')


class ShortOrganizationSerializer(serializers.ModelSerializer):
    contacts = ContactSerializer(many=True)

    class Meta:
        model = Organization
        fields = ('title', 'email', 'status', 'is_ngo',
                  'website', 'facebook', 'twitter', 'contacts')


class LocationSerializer(serializers.ModelSerializer):
    organization = ShortOrganizationSerializer(read_only=True)
    address = AddressSerializer(read_only=True)

    class Meta:
        model = Location
        fields = ('title', 'latitude', 'longitude', 'open_hour', 'close_hour',
                  'is_permanent', 'valid_until', 'organization', 'address')


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('iso_code_1', 'iso_code_2', 'name')
