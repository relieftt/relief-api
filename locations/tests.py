from django.test import TestCase
from .models import *

# Model Test Cases
class ApiModelTest(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        cls.country = Country.objects.create(
            iso_code_1=100,
            iso_code_2='AB',
            name='Test Country'
        )
        cls.region = Region.objects.create(
            name='Test Region',
            cardinal_point='N',
            country=cls.country
        )
        cls.city = City.objects.create(
            name='Test City',
            region=cls.region
        )
        cls.org_address = Address.objects.create(
            address_1='Address Line 1',
            address_2='Address Line 2',
            city=cls.city
        )
        cls.loc_address = Address.objects.create(
            address_1='Address Line 1',
            address_2='Address Line 2',
            city=cls.city
        )
        cls.ngo_org = Organization.objects.create(
            title='NGO Organization',
            is_ngo=True,
            email='ngo@ngo.com',
            website='http://ngo.com',
            facebook='https://www.facebook.com/ngo',
            twitter='https://www.twitter.com/ngo',
            primary_address=cls.org_address
        )
        cls.private_org = Organization.objects.create(
            title='Private Organization',
            email='private@private.com',
            website='http://private.com',
            facebook='https://www.facebook.com/private',
            twitter='https://www.twitter.com/private',
            primary_address=cls.org_address
        )
        cls.ngo_location = Location.objects.create(
            title='NGO Location',
            latitude = 1.0,
            longitude = 1.0,
            is_permanent = True,
            organization = cls.ngo_org,
            address = cls.loc_address
        )
        cls.private_location = Location.objects.create(
            title='Private Location',
            latitude = 1.0,
            longitude = 1.0,
            is_permanent = True,
            organization = cls.private_org,
            address = cls.loc_address
        )
        cls.contact_1 = Contact.objects.create(
            name = 'John Doe',
            phone_area_code = 555,
            phone_prefix = 555,
            phone_line_code = 5555,
            organization = cls.ngo_org
        )
        cls.contact_2 = Contact.objects.create(
            name = 'Jane Doe',
            phone_area_code = 555,
            phone_prefix = 555,
            phone_line_code = 5555,
            organization = cls.private_org
        )
        cls.contact_3 = Contact.objects.create(
            name = 'Jeffery Doe',
            phone_area_code = 555,
            phone_prefix = 555,
            phone_line_code = 5555,
            organization = cls.private_org
        )
        cls.test_1_donation = Donation.objects.create(
            item = 'Cans',
            description = 'Test Cans'
        )
        cls.test_2_donation = Donation.objects.create(
            item = 'Sheets',
            description = 'Test Sheets'
        )
        cls.test_3_donation = Donation.objects.create(
            item = 'Dry Food',
            description = 'Test Dry Food'
        )
        cls.test_1_donation.organizations.add(cls.ngo_org)
        cls.test_1_donation.organizations.add(cls.private_org)
        cls.test_2_donation.organizations.add(cls.private_org)
        cls.test_3_donation.organizations.add(cls.private_org)
        cls.test_3_donation.organizations.add(cls.ngo_org)


    def test_organization_creation(self):
        self.assertIsInstance(self.ngo_org,Organization)
        self.assertEqual(self.ngo_org.__str__(), 'NGO Organization [NGO] (Status: Pending)')
        self.assertEqual(self.ngo_org.is_ngo, True)
        self.assertNotEqual(self.private_org.is_ngo, True)
        self.assertIsInstance(self.private_org, Organization)
        self.assertEqual(self.private_org.__str__(), 'Private Organization [Private] (Status: Pending)')
        self.assertEqual(self.private_org.is_ngo, False)
        self.assertNotEqual(self.ngo_org.is_ngo, False)

    def test_location_creation(self):
        self.assertIsInstance(self.ngo_location, Location)
        self.assertEqual(self.ngo_location.__str__(),'NGO Location (NGO Organization)')
        self.assertIsInstance(self.private_location, Location)
        self.assertEqual(self.private_location.__str__(),'Private Location (Private Organization)')
    

    def test_address_creation(self):
        self.assertIsInstance(self.org_address, Address)
        self.assertEqual(self.org_address.__str__(),'Address Line 1, Address Line 2, Test City, Test Region, Test Country')
        self.assertIsInstance(self.loc_address, Address)
        self.assertEqual(self.loc_address.__str__(),'Address Line 1, Address Line 2, Test City, Test Region, Test Country')


    def test_contact_creation(self):
        self.assertIsInstance(self.contact_1, Contact)
        self.assertEqual(self.contact_1.__str__(),'John Doe : +1 (555) 555-5555')
        self.assertIsInstance(self.contact_2, Contact)
        self.assertEqual(self.contact_2.__str__(),'Jane Doe : +1 (555) 555-5555')
        self.assertIsInstance(self.contact_3, Contact)
        self.assertEqual(self.contact_3.__str__(),'Jeffery Doe : +1 (555) 555-5555')


    def test_donation_creation(self):
        self.assertIsInstance(self.test_1_donation, Donation)
        self.assertEqual(self.test_1_donation.__str__(),'Cans')
        self.assertIsInstance(self.test_2_donation, Donation)
        self.assertEqual(self.test_2_donation.__str__(),'Sheets')
        self.assertIsInstance(self.test_3_donation, Donation)
        self.assertEqual(self.test_3_donation.__str__(),'Dry Food')