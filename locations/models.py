import datetime
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from model_utils.fields import StatusField
from model_utils import Choices
from django_extensions.db.fields import AutoSlugField

class Country(models.Model):
    iso_code_1 = models.IntegerField(primary_key=True, verbose_name=_('iso code 1'))
    iso_code_2 = models.CharField(max_length=3, null=False, blank=False, verbose_name=_('iso code 2'))
    name = models.CharField(max_length=20, null=False, blank=False, verbose_name=_('country_name'))

    class Meta:
        ordering = ['name',]
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return '%s (%s; %d)' % (self.name, self.iso_code_2, self.iso_code_1)

class Region(models.Model):
    CARDINAL_POINTS = Choices(('N',_('North')),('S',_('South')),('E',_('East')),('W',_('West')),('C',_('Central')),('T',_('Tobago')))
    name = models.CharField(max_length=60, blank=False, null=False, verbose_name=_('name'))
    slug = AutoSlugField(populate_from=['name',])
    cardinal_point = models.CharField(max_length=1, blank=False, null=False, choices=CARDINAL_POINTS, verbose_name=_('cardinal_point'))
    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name='country_region')

    class Meta:
        ordering = ['name',]
        verbose_name = _('Region')
        verbose_name_plural = _('Regions')

    @property
    def country_name(self):
        return self.country.name


    def __str__(self):
        return '%s (%s)' % (self.name, self.country_name)

class City(models.Model):
    name = models.CharField(max_length=60, blank=False, null=False, verbose_name=_('name'))
    slug = AutoSlugField(populate_from=['name',])
    zip_code = models.CharField(max_length=10, default='00000')
    region = models.ForeignKey(Region, on_delete=models.CASCADE, related_name='city_region')

    class Meta:
        ordering = ['name',]
        verbose_name = _('City')
        verbose_name_plural = _('Cities')


    @property
    def region_name(self):
        return self.region.name

    @property
    def country_name(self):
        return self.region.country_name

    def __str__(self):
        return '%s, %s - %s (%s)' % (self.name, self.zip_code, self.region_name, self.country_name)
class Address(models.Model):
    address_1 = models.CharField(max_length=60, blank=False, null=False, verbose_name=_('address line 1'))
    address_2 = models.CharField(max_length=60, blank=True, null=True, verbose_name=_('address line 2'))
    city = models.ForeignKey(City, related_name='city')

    class Meta:
        ordering = ['city',]
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')

    @property
    def city_name(self):
        return self.city.name


    @property
    def region_name(self):
        return self.city.region_name


    @property
    def country_name(self):
        return self.city.country_name


    def __str__(self):
        return '%s, %s, %s, %s, %s' % (self.address_1, self.address_2, self.city_name, self.region_name, self.country_name)


class Organization(models.Model):
    VERIFY_STATUS = Choices(('P', _('Pending')), ('V', _('Verified')), ('N', _('Not Active')),)
    title = models.CharField(max_length=40, blank=False, null=False, unique=True, verbose_name=_('title'))
    slug = AutoSlugField(populate_from=['title',])
    is_ngo = models.BooleanField(default=False, verbose_name=_('ngo?'))
    verify_status = StatusField(choices_name='VERIFY_STATUS', default=VERIFY_STATUS.P)
    email = models.EmailField(null=True, blank=False, verbose_name=_('email'))
    website = models.URLField(null=True, blank=True, verbose_name=_('website'))
    facebook = models.URLField(null=True, blank=True, verbose_name=_('facebook'))
    twitter = models.URLField(null=True, blank=True, verbose_name=_('twitter'))
    primary_address = models.ForeignKey(Address, related_name='primary_address')


    class Meta:
        ordering = ['title', 'verify_status',]
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')


    def ngo_status(self):
        if self.is_ngo:
            return 'NGO'
        return 'Private'


    @property
    def status(self):
        return self.VERIFY_STATUS[self.verify_status]


    def __str__(self):
        return '%s [%s] (Status: %s)' % (self.title, self.ngo_status(), self.status)


def two_weeks():
    today = timezone.now().replace(hour=0, minute=0)
    return today + timezone.timedelta(days=14)

    
class Location(models.Model):
    title = models.CharField(max_length=40, blank=False, null=False, verbose_name=_('title'))
    latitude = models.FloatField(null=False, blank=False, verbose_name=_('latitude'))
    longitude = models.FloatField(null=False, blank=False, verbose_name=_('longitude'))
    open_hour = models.TimeField(default=datetime.time(8,0,0), verbose_name=_('opening hour'))
    close_hour = models.TimeField(default=datetime.time(16,30,0), verbose_name=_('closing hour'))
    is_permanent = models.BooleanField(default=True, verbose_name=_('permanent?'))
    valid_until = models.DateField(default=two_weeks, verbose_name=_('valid until'))
    organization = models.ForeignKey(Organization, related_name='organization')
    address = models.ForeignKey(Address, related_name='address')


    class Meta:
        ordering = ['title', '-is_permanent',]
        verbose_name = _('Location')
        verbose_name_plural = _('Locations')


    def get_organization(self):
        return self.organization.title


    def __str__(self):
        return '%s (%s)' % (self.title, self.get_organization())


class Contact(models.Model):
    name = models.CharField(max_length=40, verbose_name=_('name'))
    phone_call_code = models.CharField(max_length=3, default='+1', verbose_name=_('phone call code'))
    phone_area_code = models.PositiveSmallIntegerField(null=False, blank=False, verbose_name=_('phone area code'))
    phone_prefix = models.PositiveSmallIntegerField(null=False, blank=False, verbose_name=_('phone prefix'))
    phone_line_code = models.PositiveSmallIntegerField(null=False, blank=False, verbose_name=_('phone line code'))
    sms_allowed = models.BooleanField(default=False, verbose_name=_('sms?'))
    whatsapp_allowed = models.BooleanField(default=False, verbose_name=_('whatsapp?'))
    snapchat_allowed = models.BooleanField(default=False, verbose_name=_('snapchat?'))
    telegram_allowed = models.BooleanField(default=False, verbose_name=_('telegram?'))
    organization = models.ForeignKey(Organization, related_name='contacts')


    class Meta:
        ordering = ['name',]
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')


    def __str__(self):
        return '%s : %s (%d) %d-%d' % (self.name, self.phone_call_code, self.phone_area_code, self.phone_prefix, self.phone_line_code)


class Donation(models.Model):
    item = models.CharField(max_length=20, null=False, blank=False, verbose_name=_('item'))
    description = models.TextField(null=False, blank=False, verbose_name=_('description'))
    organizations = models.ManyToManyField(Organization, related_name='donations')


    class Meta:
        ordering = ['item',]
        verbose_name = _('Donation')
        verbose_name_plural = _('Donations')


    def __str__(self):
        return '%s' % (self.item)