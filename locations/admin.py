from django.contrib import admin
from django.contrib.auth.models import Group
from .models import *

# Unregister Group Object
admin.site.unregister(Group)

# Relief TT branding
admin.site.site_header = 'Relief TT API Backend'
admin.site.site_title = 'Relief TT'

# Register your models here.


class CityInline(admin.StackedInline):
    model = City
    extra = 1


class RegionInline(admin.TabularInline):
    model = Region
    extra = 1


class ContactInline(admin.TabularInline):
    model = Contact
    extra = 1


class DonationInline(admin.TabularInline):
    model = Donation.organizations.through
    extra = 1


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ['name', 'iso_code_1', 'iso_code_2']
    list_display_links = ['name']
    search_fields = ['name', ]
    inlines = [
        RegionInline,
    ]
    fields = ('name', ('iso_code_1', 'iso_code_2'))


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ['name', 'cardinal_point', 'country']
    list_display_links = ['name']
    list_filter = ['cardinal_point', 'country']
    inlines = [
        CityInline,
    ]
    fields = ('name', 'cardinal_point', 'country')


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ['city', 'address_1', 'address_2', ]
    list_display_links = ['city', ]
    list_filter = ['city', ]
    fields = ('address_1', 'address_2', 'city',)


@admin.register(Donation)
class DonationAdmin(admin.ModelAdmin):
    list_display = ['item', 'description']
    list_display_links = ['item', ]
    fields = ('item', 'description')


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ['slug', 'title',
                    'primary_address', 'is_ngo', 'verify_status']
    list_display_links = ['slug']
    list_filter = ['title', 'is_ngo', 'verify_status']
    search_fields = ['title']
    inlines = [
        ContactInline,
        DonationInline,
    ]
    fieldsets = (
        ('General Information', {
            'fields': ('title', ('is_ngo', 'verify_status'), 'primary_address'),
        }),
        ('Web Information', {
            'fields': (('email', 'website'), ('facebook', 'twitter')),
        }),
    )


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ['title', 'organization', 'address']
    list_display_links = ['title', ]
    list_filter = ['organization', 'is_permanent']
    search_fields = ['title', 'organization']
    fieldsets = (
        ('General', {
            'fields': ('title', 'organization',)
        }),
        ('Opening Hours', {
            'fields': ('open_hour', 'close_hour',)
        }),
        ('Location Options', {
            'fields': (('latitude', 'longitude'),
                       ('is_permanent', 'valid_until'), 'address')
        })
    )
