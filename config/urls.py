from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from rest_framework import routers, permissions
from rest_framework_nested import routers
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from locations import views

# API Schema information
schema_view = get_schema_view(
    openapi.Info(
        title='ReliefTT API',
        default_version='v1',
        description='ReliefTT API Backend',
        contact=openapi.Contact(email='reliefttdev@gmail.com'),
        license=openapi.License(name='MIT License'),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

# API Router
router = routers.DefaultRouter()
router.register(r'', views.ApiRootViewSet, basename='api-root')
router.register(r'organization', views.OrganizationViewSet,
                basename='organization')
router.register(r'location', views.LocationViewSet, basename='location')
router.register(r'country', views.CountryViewSet, basename='country')

organization_router = routers.NestedSimpleRouter(
    router, r'organization', lookup='organization')
organization_router.register(
    r'contact', views.OrganizationContactViewSet, base_name='organization_contacts')
organization_router.register(
    r'donation', views.OrganizationDonationViewSet, base_name='organization_donations')
organization_router.register(
    r'location', views.OrganizationLocationViewSet, base_name='organization_locations')

country_router = routers.NestedSimpleRouter(
    router, r'country', lookup='country')
country_router.register(
    r'location', views.CountryLocationViewSet, base_name='country_locations')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/', include(organization_router.urls)),
    url(r'^api/v1/', include(country_router.urls)),
    url(r'^swagger(?P<format>\.json)$', schema_view.without_ui(
        cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger',
        cache_timeout=0), name='schema-swagger-ui'),
    url(r'^$', TemplateView.as_view(template_name='index.html')),
]
