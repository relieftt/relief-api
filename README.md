# Relief-API

API web server backend for the ReliefTT project.

[![forthebadge made-with-python][6]](https://www.python.org/)

![python][1] ![coverage][2] ![bandit][3] ![tests][4] [![MIT license][5]](https://lbesson.mit-license.org/)

## Installation

### Dependencies

- Python = 3.6
- Django = 1.11.17
- Django Rest Framework = 3.9.4
- Django CORS Headers = 3.1.1
- Python Decouple = 3.1
- Django Model Utils = 3.2.0
- Django Extensions = 2.1.9
- dj-database-url = 0.5.0
- drf-yasg = 1.17
- coverage = 4.5.3
- bandit = 1.6.2
- drf-nested-frameworks = 0.92.3

### Steps

Clone the repo and navigate to the working directory.

```bash
git clone https://bitbucket.org/relieftt/relief-api.git
cd relief-api
```

Use virtualenv to create a virtual environment and activate it.

```bash
virtualenv venv
# For Linux/Unix Users
source ./venv/Scripts/activate

# For Windows Users
.\venv\Scripts\activate.ps1
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the dependencies.

```bash
pip install -r requirements.txt
```

_Note_: This project uses [python decouple](https://pypi.org/project/python-decouple/) and [dj-database-url](https://pypi.org/project/dj-database-url/) to configure the environment variables for this project.

Create a `.env` file in the working directory by copying and renaming the `.env_example` file

```bash
# For Linux/Unix Users
cp .env_example .env

# For Windows Users
copy .env_example .env
```

Edit the variables in the `.env` file to either:

- Connect to your chosen database;
- Enable or Disable debugging mode;
- Change the Secret Key; or
- Add any additional environment variables for the Django Web Server.

## Usage

Create a superuser by entering the following command. _Note_: This is required to access the default Django Admin site.

```bash
python manage.py createsuperuser
```

Start the Django Web server.

```bash
python manage.py runserver
```

Navigate to <http://localhost:8000> to browse. API Documentation can be found at <http://localhost:8000/swagger>. Data entry is done through the Django Admin at <http://localhost:8000/admin>

To run tests, execute the following command:

```bash
python manage.py test
```

To run [Coverage](https://pypi.org/project/coverage/) and [Bandit](https://pypi.org/project/bandit/) security tests, execute the following commands:

```bash
# Coverage
coverage run manage.py test
# Bandit
bandit -r .
```

_For Production ONLY:_

Collect static files by using the following command:

```bash
python manage.py collectstatic
```

Change `DEBUG` in the `.env` file to `False`.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)

[1]: https://img.shields.io/badge/python-3.6-blue
[2]: https://img.shields.io/badge/coverage-92%25-green
[3]: https://img.shields.io/badge/bandit-0%20issues-green
[4]: https://img.shields.io/badge/tests-passing-green
[5]: https://img.shields.io/badge/License-MIT-blue.svg
[6]: http://ForTheBadge.com/images/badges/made-with-python.svg
